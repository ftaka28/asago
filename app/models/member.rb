class Member < ApplicationRecord
  class << self
    def search(query)
      rel = order("number")
      if query.present?
        rel = rel.where("name LIKE ? or full_name LIKE ?",
          "%#{query}%", "%#{query}%")
      end
    end
  end
end
